provider "aws" {
    region = "eu-west-1"
    profile = "${terraform.workspace}"
}
# Main Module for the Prod VPC Environment
resource "aws_vpc" "vpc_prod" {
  cidr_block                       = var.cidr
  instance_tenancy                 = var.instance_tenancy
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  enable_classiclink               = var.enable_classiclink

  tags = {
      Name = var.tags
    }

}
