resource "aws_subnet" "public_1" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = true
  cidr_block = "172.31.24.0/24"

  tags = {
    Name = "public_1-prod"
  }
}
resource "aws_subnet" "public_2" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = true
  cidr_block = "172.31.25.0/24"

  tags = {
    Name = "public_2-demo"
  }
}
resource "aws_subnet" "public_3" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = true
  cidr_block = "172.31.26.0/24"

  tags = {
    Name = "public_3-prod"
  }
}
