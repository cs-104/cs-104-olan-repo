provider "aws" {
  version = "~>2.0"
  region  = "eu-west-1"
  profile = terraform.workspace

}

module "ec2_instance" {
  source = "./ec2-module"
}
