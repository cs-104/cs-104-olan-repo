resource "aws_vpc" "dev-minds" {
  cidr_block           = var.cidr
  instance_tenancy     = var.instance_tenancy
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name = var.tags
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.dev-minds.id

  tags = {
    Name = "internet-gateway-olan"
  }
}

resource "aws_subnet" "public_1" {
  vpc_id                  = aws_vpc.dev-minds.id
  map_public_ip_on_launch = true
  cidr_block              = "192.168.8.0/24"

  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = "public_1-olan"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id                  = aws_vpc.dev-minds.id
  map_public_ip_on_launch = false
  cidr_block              = "192.168.9.0/24"

  tags = {
    Name = "private_1-olan"
  }
}

resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.dev-minds.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public-route-table-olan"
  }
}

#resource "aws_eip" "bar" {
#    vpc = true
#  
#    instance                  = aws_instance.dev-minds.id
#    associate_with_private_ip = aws_subnet.private_1.id
#    depends_on                = [aws_internet_gateway.gw]
#}


resource "aws_route_table_association" "public_1" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.route-public.id
}
