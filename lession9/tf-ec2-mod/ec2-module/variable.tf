variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "ami_id" {
  type    = string
  default = "ami-04f5641b0d178a27a"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "key_name" {
  type    = string
  default = "olankey"
}

variable "cidr" {
  description = "The CIDR Block for the VPC"
  type        = string
  default     = "192.168.0.0/16"
}

variable "instance_tenancy" {
  description = "Tenancy option for the EC2 Instance"
  type        = string
  default     = "default"
}

variable "enable_dns_hostnames" {
  description = " True to enable DNS hostnames in the VPC"
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "True to enable DNS support in the VPC"
  type        = bool
  default     = true
}

variable "tags" {
  description = "Map of tags to add to all resources"
  type        = string
  default     = "olan-dev-minds"
}


#variable "subnet_id" {
#   description = "dminds-staging-env-infra-vpc"
#   type        = string
#   default     = "vpc-0e82695bfd0c584ed"
#}

