resource "aws_instance" "dev-minds" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = aws_subnet.public_1.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  tags = {
    name = "EC2-Olan"
  }

  provisioner "remote-exec" {
    inline = ["sudo yum install python3 -y", "echo Finish!"]

    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "centos"
      private_key = file("./olankey.pem")
      timeout     = "5m"
    }
  }

  provisioner "local-exec" {
    command = "sleep 30; ansible-playbook -u centos -i '${self.public_ip},' --private-key olankey.pem nginx-install.yml"
  }
}
