provider "aws" {
  version = "~>2.0"
  region  = "eu-west-1"
  profile = terraform.workspace

}

module "ami_module" {
  source = "./ami-module"
}

