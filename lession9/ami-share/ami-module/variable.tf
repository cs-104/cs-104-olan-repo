variable "region" {
  description = "AWS region" 
  default     = "eu-west-1"
 }

variable "ami_id" {
  description  = "Olan EC2 AMI"
  default      = "ami-01c2c9dc3a87416a6"
 }

 variable "account_ids" {
   description = "List of Acccount IDs"
   type = "list"
   default = ["984463041714","023451010066","197064613889"]
 }
