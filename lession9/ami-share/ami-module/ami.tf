 resource "aws_ami_launch_permission" "share" {
  count       = "${length(var.account_ids)}"
  account_id  = "${var.account_ids[count.index]}"
  image_id     = var.ami_id
 }
