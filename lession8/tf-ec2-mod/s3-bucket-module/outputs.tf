output "s3_bucket_id" {
  value = aws_s3_bucket.state-storage.id
}

output "s3_bucket_arn" {
  value = aws_s3_bucket.state-storage.arn
}
